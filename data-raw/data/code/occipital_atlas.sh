#!/bin/bash

export SUBJECTS_DIR=$PWD/derivatives/freesurfer

for s in $(seq -f "%02g" 5 9)
do
  # Invert the right hemisphere
  xhemireg --s sub-$s
  # Register the left hemisphere to fsaverage_sym
  surfreg --s sub-$s --t fsaverage_sym --lh
  # Register the inverted right hemisphere to fsaverage_sym
  surfreg --s sub-$s --t fsaverage_sym --lh --xhemi
  # find visual regions
  docker run -ti --rm -v $PWD/derivatives/freesurfer/sub-$s:/input nben/occipital_atlas:latest

done
