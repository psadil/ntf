export SUBJECTS_DIR=$PWD/derivatives/freesurfer

for s in $(seq -f "%02g" 1 3)
do

  # # need single roi
  fslroi $PWD/derivatives/fmriprep/sub-"$s"/func/sub-"$s"_task-con_run-01_bold_space-T1w_preproc.nii.gz \
  $PWD/derivatives/fmriprep/sub-"$s"/func/sub-"$s"_task-con_run-01_TR-001_bold_space-T1w_preproc.nii.gz \
  0 1

  mri_convert -rt nearest -rl $PWD/derivatives/fmriprep/sub-"$s"/func/sub-"$s"_task-con_run-01_TR-001_bold_space-T1w_preproc.nii.gz \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_areas.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/sub-"$s"_task-con_bold_space-T1w_template-areas.nii

  mri_convert -rt nearest -rl $PWD/derivatives/fmriprep/sub-"$s"/func/sub-"$s"_task-con_run-01_TR-001_bold_space-T1w_preproc.nii.gz \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_angle.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/sub-"$s"_task-con_bold_space-T1w_template-angle.nii

  mri_convert -rt nearest -rl $PWD/derivatives/fmriprep/sub-"$s"/func/sub-"$s"_task-con_run-01_TR-001_bold_space-T1w_preproc.nii.gz \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_eccen.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/sub-"$s"_task-con_bold_space-T1w_template-eccen.nii

done
