import os

def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes

def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where
    allowed template fields - follow python string module:
    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

    t1w = create_key(os.path.join('sub-{subject}','anat', 'sub-{subject}_acq-{acq}_T1w'))
    con = create_key(os.path.join('sub-{subject}','func', 'sub-{subject}_task-con_run-{item:02d}_bold'))
    ret = create_key(os.path.join('sub-{subject}', 'func', 'sub-{subject}_task-ret_run-{item:02d}_bold'))
    loc = create_key(os.path.join('sub-{subject}', 'func', 'sub-{subject}_task-loc_run-{item:02d}_bold'))
    magnitude = create_key(os.path.join('sub-{subject}', 'fmap', 'sub-{subject}_magnitude'))
    phasediff = create_key(os.path.join('sub-{subject}', 'fmap', 'sub-{subject}_phasediff'))

    info = {t1w: [], con: [], ret: [], loc: [], magnitude: [], phasediff: []}

    for idx, s in enumerate(seqinfo):
        if ((s.dim4 == 1) and ('t1' in s.protocol_name)) or (s.dim3 == 172):
            if ('WATER_ORIG' in s.dcm_dir_name):
                info[t1w].append({'item':s.series_id,'acq': 'water'})
            if ('CFMRI' in s.example_dcm_file):
                info[t1w].append({'item':s.series_id,'acq': 'conventional'})
        if ((s.dim4 == 218) and ('retino' in s.protocol_name) and (s.TR == 1.5)) or (s.dim3 == 4200) or (s.dim3 == 4410)  or (s.dim4 == 326):
            info[ret].append(s.series_id)
        if ((s.dim4 == 208) and ('local' in s.protocol_name) and (s.TR == 1.5) and not ('19-ep2d_bold_2mm_local1' in s.series_id)) or (s.dim3 == 5460) or (s.dim4 == 320) or (s.dim4 == 336):
            info[loc].append(s.series_id)
        if ((s.dim4 == 200) and ('run' in s.protocol_name) and (s.TR == 1.5)) or (s.dim3 == 5250) or (s.dim4 == 420 and not(('9-ep2d_bold_1.8mm_run1' in s.series_id) and (20180301
 == s.date))) or (s.dim4 == 392 and '18.03.15' in s.patient_id): # sub-04 had fauly first run, sub-05 had shorted run 4
            info[con].append(s.series_id)
        if ('gre_field_mapping_sat' in s.protocol_name) and (s.TE == 3.5) and (('14-gre_field_mapping_sat' in s.series_id or '6-gre_field_mapping_sat' in s.series_id ) or ('7-gre_field_mapping_sat' in s.series_id)):
            info[magnitude].append(s.series_id)
        if ('gre_field_mapping_sat' in s.protocol_name) and (s.TE == 5.96) and (('15-gre_field_mapping_sat' in s.series_id or '7-gre_field_mapping_sat' in s.series_id)  or ('8-gre_field_mapping_sat' in s.series_id)):
            info[phasediff].append(s.series_id)

    return info
