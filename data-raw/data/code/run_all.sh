#!/bin/bash

# --------  grab latest docker images --------------
docker pull nipy/heudiconv
#docker pull singularityware/docker2singularity
docker pull poldracklab/fmriprep
docker pull nben/occipital_atlas
docker pull psadil/cmap-nipype-docker

# --------  to bids --------------

# first conversion to get info for heuristic file
docker run --rm -it -v $PWD:/data nipy/heudiconv -d '/data/sourcedata/sub-{subject}/*/*[IMA\|CFMRI]*' -s 01 02 03 -f /data/code/heuristics/convertall.py -c none -b -o /data --minmeta

# convert to bids (needs root folder)
docker run --rm -it -v $PWD:/data nipy/heudiconv -d '/data/sourcedata/sub-{subject}/*/*[IMA\|CFMRI]*' -s 05 -f /data/code/heuristics/heuristic_ntf.py -c dcm2niix -b -o /data --minmeta


# 1) fill out any extra info
# 2) Validate BIDS: http://incf.github.io/bids-validator/

# --------  fmriprep --------------

# run on server
fmriprep-docker -w $PWD/derivatives/work --fs-license-file $PWD/code/license.txt $PWD $PWD/derivatives participant --output-space template T1w fsaverage fsnative --ignore slicetiming --participant_label 05 06 07 08 09
fmriprep-docker -w $PWD/derivatives/work --fs-license-file $PWD/code/license.txt $PWD $PWD/derivatives participant --output-space template T1w fsaverage fsnative --force-no-bbr --use-syn-sdc --participant_label 02 03
fmriprep-docker -w $PWD/derivatives/work --fs-license-file $PWD/code/license.txt $PWD $PWD/derivatives participant --output-space template T1w fsaverage fsnative --use-syn-sdc --participant_label 01

# --------  retinotopy --------------

# transfer fsaverage subject to freesurfer directory
rsync -avP $SUBJECTS_DIR/fsaverage_sym $PWD/derivatives/freesurfer

# label regions, only in native space
$PWD/code/occipital_atlas.sh

# convert .mgz generated templates into .nii for resampling with ANTs (incorporate into workflow soon!)
$PWD/code/mgz_to_nii.sh

# --------  first level GLM / template to T1w --------------

# analyze data
# this is for localizer contrast + ants registration of template to T1w
docker run -it --rm -v $PWD/code/workflows:/home/neuro/workflows -v $PWD:/data -v $PWD/derivatives:/output -p 8888:8888 psadil/cmap-nipype-docker jupyter notebook
# TO TRY: jupyter nbconvert mynotebook.ipynb --stdout, or just PyCharm?
