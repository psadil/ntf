export SUBJECTS_DIR=$PWD/derivatives/freesurfer

# this script needs to be run prior to the ipython notebook (ANTS is used to regions
# the regions in space-T1w, but ANTS can't read mgz files
for s in $(seq -f "%02g" 1 9)
do

  mri_convert \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_areas.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_areas.nii

  mri_convert \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_angle.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_angle.nii

  mri_convert \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_eccen.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/native.template_eccen.nii

  mri_convert \
  $SUBJECTS_DIR/sub-"$s"/mri/rawavg.mgz \
  $SUBJECTS_DIR/sub-"$s"/mri/rawavg.nii

done
