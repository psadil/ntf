import nibabel
import numpy
import pandas
import os
from bids.grabbids import BIDSLayout

def subjectinfo(subject_id, bids_dir, output_dir, task_id):

    import pandas as pd
    from nipype.interfaces.base import Bunch
    import os
    from bids.grabbids import BIDSLayout

    subject_info = []

    layout = BIDSLayout(bids_dir, exclude="work|sourcedata")
    run_list = layout.get_runs(task = task_id, subject = subject_id)
    events_files = layout.get(subject = subject_id, task = task_id, type='events', return_type = "file")
    confounds_files = layout.get(subject = subject_id, task = task_id, type='confounds', return_type = "file")


    for r, run in enumerate(run_list):
        trialinfo = pd.read_table(events_files[r])
        conditions = []
        onsets = []
        durations = []

        for group in trialinfo.groupby('trial_type'):
            conditions.append(group[0])
            onsets.append(group[1].onset.tolist())
            durations.append(group[1].duration.tolist())


        # the last 6 columns are the motion parameters
        regress = pd.read_table(confounds_files[r]).filter(regex = 'aCompCor|X|Y|Z')

        subject_info.append(Bunch(conditions=conditions,
                          onsets=onsets,
                          durations=durations,
                          #amplitudes=None,
                          #tmod=None,
                          #pmod=None,
                          regressor_names = list(regress),
                          regressors = regress.values.T.tolist()
                         ))

    return subject_info  # this output will later be returned to infosource




# bids_dir = '/data'
bids_dir = "/home/vm01/Documents/git/ntf/data-raw/data"

subject_list = ['01', '02', '03', '05', '06', '07', '08', '09']

# layout = BIDSLayout(bids_dir)

# output_dir = '/output'
output_dir = "/home/vm01/Documents/git/ntf/data-raw/data/derivatives"
fs_dir = os.path.join(output_dir, 'freesurfer')
fmriprep_dir = os.path.join(output_dir, 'fmriprep')
scratch_dir = os.path.join(output_dir, 'work')
spm_dir = os.path.join(output_dir, 'spm')

csv_dir = os.path.join(output_dir, 'csv')


n_regressor = 6+6
n_run = 8


runs = range(8)

for sub in subject_list:

    condition_names = pandas.Categorical(subjectinfo(sub, bids_dir, output_dir, 'con')[0].get('conditions'))
    fs_sub_dir = os.path.join(fs_dir, 'sub-'+sub)
    fmriprep_sub_dir = os.path.join(fmriprep_dir, 'sub-'+sub)
    beta_dir = os.path.join(scratch_dir, 'l1analyses', '_subject_id_'+sub+'_task_id_con', 'level1estimate')

    d_list = [None for _ in range(len(runs)*len(condition_names))]
    id = 0

    for run in runs:

        areas = nibabel.load(os.path.join(fs_sub_dir, 'mri', 'sub-'+sub+'_space-T1w_template-areas.nii'))
        eccentricity = nibabel.load(os.path.join(fs_sub_dir, 'mri', 'sub-'+sub+'_space-T1w_template-eccen.nii'))
        angle = nibabel.load(os.path.join(fs_sub_dir, 'mri', 'sub-'+sub+'_space-T1w_template-angle.nii'))
        aparcaseg = nibabel.load(os.path.join(fmriprep_sub_dir, 'func',
                                             'sub-'+sub+'_task-con_run-{0:02}_bold_space-T1w_label-aparcaseg_roi.nii.gz'.format(run+1)))

        if float(sub) < 4:
            n_condition = 18
            loc_spm = nibabel.load(os.path.join(scratch_dir, 'l1analyses', '_subject_id_'+sub+'_task_id_loc','level1conest' ,'spmT_0001.nii') )
        elif float(sub) >= 4:
            loc_spm = nibabel.load(os.path.join(scratch_dir, 'l1analyses', '_subject_id_'+sub+'_task_id_loc','level1conest' ,'spmF_0003.nii') )
            n_condition = 47

        beta_one_sess = numpy.arange(start=1,stop=n_condition*3, step=3)
        beta_offset = numpy.repeat((n_regressor + n_condition*3) * run, n_condition)
        betas_to_load = beta_one_sess + beta_offset


        for b, label in enumerate(betas_to_load):
            beta_img = nibabel.load(os.path.join(beta_dir, 'beta_{0:04d}.nii'.format(label) ))
            n_voxel = beta_img.get_data().size

            d_list[id] = pandas.DataFrame({'beta': pandas.Series(beta_img.get_data().ravel()),
                                           'area': pandas.Categorical(areas.get_data().ravel()),
                                           'eccentricity': pandas.Series(eccentricity.get_data().ravel()),
                                           'angle': pandas.Series(angle.get_data().ravel()),
                                           'aparcaseg': pandas.Categorical(aparcaseg.get_data().ravel()),
                                           'trial_type': condition_names[b],
                                           'run': run + 1,
                                           'voxel': range(n_voxel),
                                           'localizer_t': pandas.Series(loc_spm.get_data().ravel())
                                            })
            d_list[id] = d_list[id][(d_list[id].area == 1.) | (d_list[id].area == 2.) | (d_list[id].area == 3.)]
            d_list[id] = d_list[id][d_list[id].localizer_t > 1.64879] # comes from running qt(.95, df) in R: qt(.95, 388)
            id += 1

    d = pandas.concat(d_list, ignore_index = True)
    d.to_csv(os.path.join(csv_dir,'sub-'+sub,'sub-'+sub+'_task-con_run-01-08_area-V1-V3_space-T1w_beta.tsv'),
             index=False, sep = "\t")
