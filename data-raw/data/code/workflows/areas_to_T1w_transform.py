
from nipype.interfaces.ants import ApplyTransforms
from bids.grabbids import BIDSLayout
import os
from nipype.interfaces.fsl import ExtractROI
from nipype.interfaces.freesurfer import ApplyVolTransform
from nipype.pipeline.engine import Workflow, Node, MapNode
from nipype.interfaces.utility import Function, IdentityInterface, Select
from nipype.interfaces.freesurfer import MRIConvert

def get_input_image(subject_id, output_dir, template_type):

    import os
    fs_dir = os.path.join(output_dir, 'freesurfer')

    input_images = os.path.join(fs_dir, 'sub-{}'.format(subject_id),
                                 'mri', 'native.template_{}.mgz'.format(template_type))

    return input_images

def get_output_image(subject_id, output_dir, template_type):

    import os
    fs_dir = os.path.join(output_dir, 'freesurfer')

    output_images = os.path.join(fs_dir, 'sub-{}'.format(subject_id),
                                 'mri', 'sub-{}_space-T1w_template-{}.nii'.format(subject_id,
                                                                       template_type))
    return output_images

def get_template(subject_id, output_dir):

    import os
    fmriprep_dir = os.path.join(output_dir, 'fmriprep')

    if(subject_id in ['02','03', '06']):
        transform = [os.path.join(fmriprep_dir, 'sub-{}'.format(subject_id),
                             'anat', 'sub-{}_acq-conventional_T1w_space-orig_target-T1w_affine.txt'.format(subject_id))]
    elif(subject_id in ['01','04']):
        transform = [os.path.join(fmriprep_dir, 'sub-{}'.format(subject_id),
                             'anat', 'sub-{}_T1w_space-orig_target-T1w_affine.txt'.format(subject_id))]
    else:
        transform = [os.path.join(fmriprep_dir, 'sub-{}'.format(subject_id),
                             'anat', 'sub-{}_acq-water_T1w_space-orig_target-T1w_affine.txt'.format(subject_id))]

    return transform

def get_reference_image(subject_id, output_dir):

    import os
    fmriprep_dir = os.path.join(output_dir, 'fmriprep')

    reference_image = os.path.join(fmriprep_dir, 'sub-{}'.format(subject_id),'func',
                                   'sub-{}_task-con_run-01_bold_space-T1w_brainmask.nii.gz'.format(subject_id))

    return reference_image

def mgz_to_nii(in_file):
    import os
    import nibabel

    d = nibabel.load(in_file)
    out_file = os.path.splitext(in_file)[0] + ".nii"
    nibabel.save(d, out_file)

    return out_file


# bids_dir = '/data'
bids_dir = "/home/vm01/Documents/git/ntf/data-raw/data"

# layout = BIDSLayout(bids_dir)

# output_dir = '/output'
output_dir = "/home/vm01/Documents/git/ntf/data-raw/data/derivatives"

# ants_dir = os.path.join(output_dir, 'ants')
fs_dir = os.path.join(output_dir, 'freesurfer')
fmriprep_dir = os.path.join(output_dir, 'fmriprep')
work_dir = os.path.join(output_dir, 'work')

subject_list = ['01', '02', '03', '05', '06', '07', '08', '09']

template_type = ["areas", "angle", "eccen"]


# Infosource - a function free node to iterate over the list of subject names
infosource = Node(IdentityInterface(fields=['subject_id', 'template_type']),
                  name="infosource")
infosource.iterables = [('subject_id', subject_list),
                       ('template_type', template_type)]

# Get Subject Info - get subject specific condition information
getinputimage = Node(Function(input_names=['subject_id',  'output_dir', 'template_type'],
                               output_names=['input_image'],
                               function=get_input_image),
                      name='getinputimage')
getinputimage.inputs.output_dir = output_dir

mgz_to_nii = Node(Function(input_names = ['in_file'],
                               output_names = ['out_file'],
                               function = mgz_to_nii),
                      name='mgz_to_nii')

getoutputimage = Node(Function(input_names=['subject_id',  'output_dir', 'template_type'],
                               output_names=['output_image'],
                               function=get_output_image),
                      name='getoutputimage')
getoutputimage.inputs.output_dir = output_dir

gettemplate = Node(Function(input_names=['subject_id',  'output_dir'],
                               output_names=['transform'],
                               function=get_template),
                      name='gettemplate')
gettemplate.inputs.output_dir = output_dir

getref = Node(Function(input_names=['subject_id',  'output_dir'],
                               output_names=['reference_image'],
                               function=get_reference_image),
                      name='getref')
getref.inputs.output_dir = output_dir

at = Node(ApplyTransforms(interpolation = 'NearestNeighbor',
                             invert_transform_flags = [False]),
             name = 'template2T1w_ants_affine')


# Initiation of the 1st-level analysis workflow
template_to_t1w = Workflow(name = 'template_to_t1w')
template_to_t1w.base_dir = os.path.join(bids_dir, work_dir)

# Connect up the 1st-level analysis components
template_to_t1w.connect([(infosource, getinputimage, [('subject_id','subject_id'),
                                                 ('template_type', 'template_type')]),
                         (infosource, getoutputimage, [('subject_id','subject_id'),
                                                 ('template_type', 'template_type')]),
                         (infosource, gettemplate, [('subject_id','subject_id')]),
                         (infosource, getref, [('subject_id','subject_id')]),
                         (getinputimage, mgz_to_nii, [('input_image', 'in_file')]),
                         (mgz_to_nii, at, [('out_file', 'input_image')]),
                         (getref, at, [('reference_image', 'reference_image')]),
                         (getoutputimage, at, [('output_image', 'output_image')]),
                         (gettemplate, at, [('transform', 'transforms')])
                   ])


# template_to_t1w.run('MultiProc', plugin_args={'n_procs': 8})
template_to_t1w.run()
