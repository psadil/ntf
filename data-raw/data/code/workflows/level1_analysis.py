
from nipype.interfaces.spm import Level1Design, EstimateModel, EstimateContrast
from nipype.algorithms.modelgen import SpecifySPMModel
from nipype.interfaces.utility import Function, IdentityInterface, Select
from nipype.interfaces.io import SelectFiles, DataSink
from nipype.pipeline.engine import Workflow, Node, MapNode
from bids.grabbids import BIDSLayout
from nipype import DataGrabber
from nipype.algorithms.misc import Gunzip
import os

def get_first(in_file):
    return in_file[0]

def get_TR(bids_dir, subject_id, task_id):
    from bids.grabbids import BIDSLayout

    layout = BIDSLayout(bids_dir, exclude="derivatives|sourcedata")

    TR = layout.get_metadata(layout.get(subject = subject_id, run = "1",
                                        type = "bold", task = task_id, return_type = "file")[1])['RepetitionTime']

    return TR


def get_niftis(subject_id, data_dir, task_id, bids_dir):
    import os
    from bids.grabbids import BIDSLayout

    layout = BIDSLayout(bids_dir, exclude="derivatives|sourcedata")
    run_list = layout.get_runs(task = task_id, subject = subject_id)

    functional_runs = []

    for r, run in enumerate(run_list):
        functional_runs.append(os.path.join(data_dir,'sub-{}'.format(subject_id),'func',
                                          'sub-{}_task-{}_run-0{}_bold_space-T1w_preproc.nii.gz'.format(subject_id,
                                                                                                       task_id,
                                                                                                       run)))

    return functional_runs

def subjectinfo(subject_id, bids_dir, output_dir, task_id):

    import pandas as pd
    from nipype.interfaces.base import Bunch
    import os
    from bids.grabbids import BIDSLayout

    subject_info = []

    layout = BIDSLayout(bids_dir, exclude="work|sourcedata")
    run_list = layout.get_runs(task = task_id, subject = subject_id)
    events_files = layout.get(subject = subject_id, task = task_id, type='events', return_type = "file")
    confounds_files = layout.get(subject = subject_id, task = task_id, type='confounds', return_type = "file")


    for r, run in enumerate(run_list):
        trialinfo = pd.read_table(events_files[r])
        conditions = []
        onsets = []
        durations = []

        for group in trialinfo.groupby('trial_type'):
            conditions.append(group[0])
            onsets.append(group[1].onset.tolist())
            durations.append(group[1].duration.tolist())


        # the last 6 columns are the motion parameters
        regress = pd.read_table(confounds_files[r]).filter(regex = 'aCompCor|X|Y|Z')

        subject_info.append(Bunch(conditions=conditions,
                          onsets=onsets,
                          durations=durations,
                          #amplitudes=None,
                          #tmod=None,
                          #pmod=None,
                          regressor_names = list(regress),
                          regressors = regress.values.T.tolist()
                         ))

    return subject_info  # this output will later be returned to infosource


def set_contrasts(subject_id, bids_dir, output_dir, task_id, layout):
    import pandas as pd

    if task_id == 'loc':
        if float(subject_id) < 4:
            condition_names = ['checkerboard']
            cont01 = ['on', 'T', condition_names, [1]]
            contrast_list = [cont01]
        elif float(subject_id) >= 4:
            condition_names = ['checkerboard_side-left','checkerboard_side-right', 'dim_side-middle']
            cont01 = ['left', 'T', condition_names, [1, 0, 0]]
            cont02 = ['right', 'T', condition_names, [0, 1, 0]]
            cont03 = ['on', 'F', [cont01, cont02]]
            contrast_list = [cont01, cont02, cont03]

    elif task_id == 'con':
        if float(subject_id) < 4:
            condition_names = ['con-0.3_ori-20']
            cont01 = ['con-0.3_ori-20', 'T', condition_names, [1]]
            contrast_list = [cont01]
        elif float(subject_id) >= 4:
            condition_names = ['dim']
            cont01 = ['dim', 'T', condition_names, [1]]
            contrast_list = [cont01]



    return contrast_list


## specify input

# bids_dir = '/data'
bids_dir = "/home/vm01/Documents/git/ntf/data-raw/data"

layout = BIDSLayout(bids_dir, exclude="work|sourcedata")

# output_dir = '/output'
output_dir = "/home/vm01/Documents/git/ntf/data-raw/data/derivatives"
spm_dir = os.path.join(output_dir, 'spm')
fmriprep_dir = os.path.join(output_dir, 'fmriprep')
scratch_dir = os.path.join(output_dir, 'work')

# list of subject identifiers
subject_list = ['01', '02', '03', '05', '06', '07', '08', '09']
task_list = ['loc', 'con']


# SpecifyModel - Generates SPM-specific Model
modelspec = Node(SpecifySPMModel(concatenate_runs=False,
                                 input_units='secs',
                                 output_units='secs',
                                 high_pass_filter_cutoff=128),
                 name="modelspec")

# Level1Design - Generates an SPM design matrix
level1design = Node(Level1Design(bases={'hrf': {'derivs': [1, 1]}},
                                 timing_units='secs',
                                 model_serial_correlations='AR(1)'),
                    name="level1design")

# EstimateModel - estimate the parameters of the model
level1estimate = Node(EstimateModel(estimation_method={'Classical': 1}),
                      name="level1estimate")

# EstimateContrast - estimates contrasts
level1conest = Node(EstimateContrast(), use_derivs=True, name="level1conest")

# Get Subject Info - get subject specific condition information
getsubjectinfo = Node(Function(input_names=['subject_id', 'bids_dir', 'output_dir', 'task_id'],
                               output_names=['subject_info'],
                               function=subjectinfo),
                      name='getsubjectinfo')
getsubjectinfo.inputs.bids_dir = bids_dir
getsubjectinfo.inputs.output_dir = output_dir


# Infosource - a function free node to iterate over the list of subject names
infosource = Node(IdentityInterface(fields=['subject_id', 'task_id']),
                  name="infosource")
infosource.iterables = [('subject_id', subject_list),
                       ('task_id', task_list)]

level1defcontrast = Node(Function(input_names=['subject_id', 'bids_dir', 'output_dir', 'task_id', 'layout'],
                               output_names=['contrast_list'],
                               function=set_contrasts),
                      name='level1defcontrast')
level1defcontrast.inputs.bids_dir = bids_dir
level1defcontrast.inputs.output_dir = output_dir
level1defcontrast.inputs.layout = layout

getTR = Node(Function(function=get_TR,
                                input_names=["bids_dir", "subject_id", "task_id"],
                                output_names=["TR"]), name="getTR")
getTR.inputs.bids_dir = bids_dir

BIDSDataGrabber = Node(Function(function=get_niftis,
                                input_names=["subject_id","data_dir", "task_id", "bids_dir"],
                                output_names=["functional_runs"]), name="BIDSDataGrabber")
BIDSDataGrabber.inputs.data_dir = fmriprep_dir
BIDSDataGrabber.inputs.bids_dir = bids_dir

# Datasink - creates output folder for important outputs
datasink = Node(DataSink(base_directory = spm_dir), name="datasink")

# SPM can't read .nii.gz files!
gunzip_con = MapNode(Gunzip(), name="gunzip_con", iterfield=['in_file'])

# Use the following DataSink output substitutions
substitutions = [('_subject_id_%s_task_id_%s' % (sub,task), 'sub-%s/task-%s' % (sub,task))
                 for sub in subject_list
                 for task in task_list]
datasink.inputs.substitutions = substitutions



## Workflow

# Initiation of the 1st-level analysis workflow
l1analysis = Workflow(name = 'l1analyses')
l1analysis.base_dir = os.path.join(bids_dir, scratch_dir)

# Connect up the 1st-level analysis components
l1analysis.connect([(infosource, getsubjectinfo, [('subject_id','subject_id'),
                                                 ('task_id', 'task_id')]),
                    (infosource, BIDSDataGrabber, [('subject_id','subject_id'),
                                                 ('task_id', 'task_id')]),
                    (infosource, getTR, [('subject_id', 'subject_id'),
                                        ('task_id', 'task_id')]),
                    (infosource, level1defcontrast, [('subject_id', 'subject_id'),
                                        ('task_id', 'task_id')]),
                    (BIDSDataGrabber, gunzip_con, [('functional_runs', 'in_file')]),
                    (gunzip_con, modelspec, [('out_file', 'functional_runs')]),
                    (getTR, modelspec, [('TR', 'time_repetition')]),
                    (getTR, level1design, [('TR', 'interscan_interval')]),
                    (getsubjectinfo, modelspec, [('subject_info','subject_info')]),
                    (modelspec, level1design, [('session_info','session_info')]),
                    (level1design, level1estimate, [('spm_mat_file','spm_mat_file')]),
                    (level1defcontrast, level1conest, [('contrast_list', 'contrasts')]),
                    (level1estimate, level1conest, [('spm_mat_file','spm_mat_file'),
                                                    ('beta_images', 'beta_images'),
                                                    ('residual_image','residual_image')])
                    # (level1conest, datasink, [('spm_mat_file', 'spm_mat'),
                    #                           ('spmT_images', 'T'),
                    #                           ('con_images', 'con'),
                    #                           ('ess_images', 'ess'),
                    #                           ('beta_images', 'beta_images'),
                    #                           ('residual_image','residual_image')
                    #                           ])
                    ])


## run
l1analysis.run('MultiProc', plugin_args={'n_procs': 8})
# l1analysis.run()
