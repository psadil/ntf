#!/bin/bash

module load singularity/singularity-2.4.5

#BSUB -n 42
#BSUB -R rusage[mem=2048] # ask for 1GB per job slot, or 36GB total
#BSUB -W 2:00
#BSUB -q long # which queue we want to run in
#BSUB -R "span[hosts=1]" # All job slots on the same node (needed for threaded applications)
#BSUB -J d3VMM1

singularity exec psadil-ntf-master-latest.simg Rscript -e "library(ntf); data('d'); setup_job(participants = 5:9, d = d, n_channels_assumed = 6, jobs = 5, n_chains = 8, iter = 500, warmup = 1000, max_treedepth = 12, adapt_delta = .8, thresh = 150, flag = 'd3VMM1', k = 4, rescale = TRUE)"
