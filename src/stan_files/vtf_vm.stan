#include /chunks/license.stan

functions{
	// vector calc_vox_avg(vector mu, int n_voxel, int max_nv, int n, int[] voxel, vector n_v_real);
#include /functions/common_functions.stan
	// vector von_mises_pdf(vector ori_tested, real ori_pref, real kappa );
#include /functions/channel_responses.stan
}
data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	vector[n] y;
	matrix[n, n_orientation_tested * n_contrast * n_channels ] X;
	vector<lower=-pi(), upper=pi()>[n_channels] pref_ori;
	matrix[n_orientation_tested, n_channels] unique_orientation_tested;
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	int K = cols(X);
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;

	for(i in 1:n){
		n_v_int[voxel[i]] = n_v_int[voxel[i]] + 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);
}
parameters {
	// row_vector<lower = 0>[n_voxel] voxel_b_raw;
	// real<lower = 0> voxel_sigma;
	matrix[n_channels-1, n_voxel] channel_W_raw;
	real<lower = 0> sigma;
	vector<lower = 0>[n_contrast] m;
	vector<lower = 0>[n_contrast] s;
	vector<lower = 0>[n_contrast] a;
	real intercept;
	vector[n_voxel] voxel_a_raw;
	real<lower = 0> voxel_sigma_a;
}
transformed parameters {
	matrix<lower = 0, upper = 1>[n_channels, n_voxel] channel_W;
	// row_vector<lower = 0>[n_voxel] voxel_b = voxel_b_raw * voxel_sigma;
	vector[n_voxel] voxel_a = voxel_a_raw * voxel_sigma_a;

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[ ,v] = softmax(col(channel_W_tmp, v));
	}

}
model{

	// prior
	intercept ~ normal(0, priors[2]);
	m ~ lognormal(priors[1], priors[3]);
	s ~ lognormal(priors[8], priors[9]);
	a ~ gamma(2, priors[4]);

	to_vector(channel_W_raw) ~ normal(0, priors[5]);
	// voxel_sigma ~ gamma(2, priors[6]);
	// voxel_b_raw ~ normal(0, 1);
	voxel_sigma_a ~ gamma(2, priors[6]);
	voxel_a_raw ~ normal(0, 1);

	sigma ~ gamma(2, priors[7]);

	// likelihood
	{
		matrix[n_orientation_tested, n_contrast] channel_resp;
		matrix[K, n_voxel] aW;
		matrix[n_orientation_tested * n_contrast, n_channels] aW_tmp;
		// matrix[n_contrast, n_voxel] voxel_b_mat;
		// vector[n] mu;
		// vector[n_voxel] vox_avg;

		// matrix[n_contrast, n_voxel] voxel_a_mat;
		// for(con in 1:n_contrast){
		// 	voxel_b_mat[con,] = voxel_b + m[con];
		// 	// voxel_a_mat[con,] = voxel_a + a[con];
		// }

		for(v in 1:n_voxel){
			for(ch in 1:n_channels){
				for(con in 1:n_contrast){
					channel_resp[, con] = m[con] * von_mises3_pdf(unique_orientation_tested[, ch], 0.0, s[con], a[con]);
				}
				aW_tmp[, ch] = to_vector(channel_resp) * channel_W[ch, v];
			}
			aW[, v] = to_vector(aW_tmp);
		}

		// mu = rows_dot_product(X, aW[,voxel]');
		// vox_avg = calc_vox_avg(mu, n_voxel, max_nv, n, voxel, n_v_real);

		y ~ normal(rows_dot_product(X, aW[,voxel]') + intercept + voxel_a[voxel], sigma);
	}
}
