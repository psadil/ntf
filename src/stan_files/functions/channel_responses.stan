/**
  * Density function of von Mises
  *
  * @param ori_tested vector of orientations at which to evaluate pdf
  * @param ori_pref center of von mises (channel's preferred orientation)
  * @param kappa concentration of von mises
  * @return density vector (response of channel to ori_tested)
  */
vector von_mises_pdf(vector ori_tested, real ori_pref, real kappa ){

  return exp(kappa * cos(ori_tested - ori_pref)) / (2 * pi() * modified_bessel_first_kind(0, kappa));
}

/**
  * Density function of modified von Mises, pure sharpening
  *
  * @param ori_tested vector of orientations at which to evaluate pdf
  * @param ori_pref center of von mises (channel's preferred orientation)
  * @param kappa concentration of von mises
  * @return density vector (response of channel to ori_tested)
  */
vector von_mises2_pdf(vector ori_tested, real ori_pref, real kappa ){

  return exp(kappa * cos(ori_tested - ori_pref)) / (2 * pi() * modified_bessel_first_kind(0, kappa) * exp(von_mises_lpdf(0 | 0, kappa)) );
}

vector eq2(vector ori_tested, real ori_pref, real kappa ){

  return (1 / kappa) * exp(-1 * square(ori_pref - ori_tested) / square(2) );
}

vector von_mises3_pdf(vector ori_tested, real ori_pref, real kappa, real additive ){

  return (exp(kappa * cos(ori_tested - ori_pref)) / (2 * pi() * modified_bessel_first_kind(0, kappa) + additive/pi())) * (1/(fma(additive,2,1))) ;
}
