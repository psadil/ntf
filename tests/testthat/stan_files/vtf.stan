data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_orientation_tested * n_contrast] X; // + 1 contrast intercept (additive) +
	matrix[n_orientation_tested * n_contrast, n_basis * n_contrast] B[n_channels]; // +1 for additive effect
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
}
parameters {
	matrix[n_channels-1, n_voxel] channel_W_raw;
	vector[n_contrast] additive;
	real<lower=0> sigma;
	vector<lower=0>[n_voxel] multiplicative_constant;
	matrix<lower=0>[3, n_contrast] a_raw; // -1 for simplex identification
}
transformed parameters{
	matrix[n_channels, n_voxel] channel_W;
	vector[n] mu;
	matrix[n_basis, n_contrast] a;

  for(con in 1:n_contrast){
		a[1:3, con] = cumulative_sum(append_row(additive[con], a_raw[1:2, con]));
		a[4:n_basis, con] = sort_desc(cumulative_sum(append_row(additive[con], col(a_raw,con) )));
	}

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = exp(log_softmax(col(channel_W_tmp, v)));
	}

	{
		matrix[n, n_channels] beta_mat;
		vector[n_contrast * n_basis] a_long = append_row(col(a,1), col(a,2));

		for(channel in 1:n_channels){
			beta_mat[:,channel] = X * (B[channel]*a_long);
		}
		mu = rows_dot_product(beta_mat, channel_W[:, voxel]') .* multiplicative_constant[voxel]; //
	}
}
model{

	// prior
	to_vector(channel_W_raw) ~ normal(0, priors[1]);
	multiplicative_constant ~ gamma(2, priors[2]);

	to_vector(a_raw) ~ exponential(priors[3]);

	additive ~ normal(0, priors[6]);

	sigma ~ gamma(2, priors[7]);

	// likelihood
	y ~ normal(mu, sigma);

}
