data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_basis * n_channels * n_contrast] X; // cycle: basis, contrast, channel, voxel
	vector[n_voxel] avg; // voxel average activity
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	int K =  cols(X);
}
parameters {
	matrix[n_channels-1, n_voxel] channel_W_raw;
	real<lower=0> sigma;
	vector<lower=0>[n_voxel] multiplicative_constant;
	vector<lower=0.0>[n_basis-1] a_low; //
	vector[n_basis] a_high; //
}
transformed parameters{
	matrix[n_channels, n_voxel] channel_W;
	vector[n] mu;
	matrix<lower=0.0>[n_basis, n_contrast] a;

	a[, 1] = cumulative_sum(append_row(1, a_low));
	a[, 2] = exp(a_high) .* cumulative_sum( append_row(1, a_low));

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = exp(log_softmax(col(channel_W_tmp, v))) * multiplicative_constant[v] - avg[v];
	}

	{
		vector[n_contrast * n_basis] a_long = to_vector(a);
		matrix[K, n_voxel] aW;

		for(v in 1:n_voxel){
			aW[, v] = to_vector(a_long * col(channel_W, v)');
		}

		mu = rows_dot_product(X, aW[,voxel]');
	}
}
model{

	// prior
	to_vector(channel_W_raw) ~ normal(0, priors[1]);
	multiplicative_constant ~ gamma(2, priors[2]);

  a_low ~ exponential(priors[3]);
	a_high ~ normal(0, priors[4]);

	sigma ~ gamma(2, priors[7]);

	// likelihood
	y ~ normal(mu, sigma);

}
