data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	// int<lower=1> n_gamma_per_contrast;
	vector[n] y;
	matrix[n, n_basis * n_channels * n_contrast + 2] X; // cycle: basis, contrast, channel, voxel
	vector[n_voxel] avg; // voxel average activity
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	int K =  cols(X);
	// int n_gamma_per_contrast_m1 = n_gamma_per_contrast - 1;
}
parameters {
	matrix[n_channels-1, n_voxel] channel_W_raw;
	real<lower=0> sigma;
	vector<lower=0>[n_voxel] multiplicative_constant;
	matrix[n_basis, n_contrast] a; //
	vector[n_contrast] additive;
}
transformed parameters{
	matrix[n_channels, n_voxel] channel_W;
	vector[n] mu;
	// matrix[n_basis, n_contrast] a;

	// for(con in 1:n_contrast){
	// 	a[1:n_gamma_per_contrast, con] = a_raw[1:n_gamma_per_contrast, con];
	// 	for(i in 1:n_gamma_per_contrast_m1){
	// 		a[n_gamma_per_contrast + i, con] = a_raw[n_gamma_per_contrast - i, con];
	// 	}
	// }

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = exp(log_softmax(col(channel_W_tmp, v))) * multiplicative_constant[v];
	}

	{
		vector[n_contrast * n_basis] a_long = to_vector(a);
		matrix[K, n_voxel] aW;

		for(v in 1:n_voxel){
			aW[, v] = append_row(additive, to_vector(a_long * col(channel_W, v)'));
		}

		mu = rows_dot_product(X, aW[,voxel]');
	}
}
model{

	// prior
	to_vector(channel_W_raw) ~ normal(0, priors[1]);
	multiplicative_constant ~ gamma(2, priors[2]);

  additive ~ normal(0, priors[4]);
	to_vector(a) ~ normal(0, priors[4]);

	sigma ~ gamma(2, priors[7]);

	// likelihood
	y ~ normal(mu, sigma);

}
