data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_basis * n_contrast * n_voxel + 2] X; // cycle: basis, contrast, channel, voxel
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	real intercept = 0.0;
}
parameters {
	matrix[n_channels-1, n_voxel] channel_W_raw;
	vector[1] additive;
	real<lower=0> sigma;
	vector<lower=0>[n_voxel] multiplicative_constant;
	matrix<lower=0>[n_basis, n_contrast] a; // -1 for simplex identification
}
transformed parameters{
	matrix[n_channels, n_voxel] channel_W;
	vector[n] mu;

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = exp(log_softmax(col(channel_W_tmp, v))) * multiplicative_constant[v] ;
	}

	{
		matrix[n_contrast * n_basis + 2, n_channels] a_mat = rep_matrix( append_row(append_row(intercept, additive), to_vector(a) ), n_channels);
		vector[n_basis * n_contrast * n_voxel + 2] beta = to_vector(a_mat * channel_W);

		mu = X * beta;
	}
}
model{

	// prior
	to_vector(channel_W_raw) ~ normal(0, priors[1]);
	multiplicative_constant ~ gamma(2, priors[2]);

	to_vector(a) ~ normal(0, priors[3]);

	additive ~ normal(0, priors[6]);

	sigma ~ gamma(2, priors[7]);

	// likelihood
	y ~ normal(mu, sigma);

}
