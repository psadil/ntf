data{
  int n;
  real<lower=0.0> rate;
}
generated quantities {
  vector<lower=0.0>[n] y_rep;
  for(i in 1:n) y_rep[i] = exponential_rng(rate);
}
