functions{
#include /functions/common_functions.stan
}
data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_basis * n_channels * n_contrast] X; // cycle: basis, contrast, channel, voxel
	real<lower=0> a_min;
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	row_vector[n_contrast] zz = rep_row_vector(0.0, n_contrast);
	int K =  cols(X);
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;

	for(i in 1:n){
		n_v_int[voxel[i]] = n_v_int[voxel[i]] + 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);

}
parameters {
	matrix<lower = 0>[n_channels, n_voxel] channel_W_raw;
	real<lower = 0> channel_sigma;
	real<lower=0.0>sigma;
	matrix[n_basis - 1, n_contrast] a_raw; //
	real<lower = 0> m;
}
transformed parameters{
	matrix<lower=0>[n_basis, n_contrast] a;
  matrix<lower = 0>[n_channels, n_voxel] channel_W = channel_W_raw * channel_sigma;
  {
		matrix[n_basis, n_contrast] a_raw_full = append_row( zz, a_raw);
		a[, 1] = cumulative_sum(softmax(col(a_raw_full, 1)));
		a[, 2] = cumulative_sum(softmax(col(a_raw_full, 2))) * m;
	}

}
model{

	// prior
	m ~ gamma(2, priors[1]);
	channel_sigma ~ gamma(2, priors[2]);
	to_vector(channel_W_raw) ~ normal(0, 1);

	to_vector(a_raw) ~ normal(0, priors[3]);
	sigma ~ gamma(2, priors[7]);

	// likelihood
	{
		vector[n_contrast * n_basis] a_long = to_vector(a);
		matrix[K, n_voxel] aW;
		vector[n] mu;
		vector[n_voxel] vox_avg;

		for(v in 1:n_voxel){
			aW[, v] = to_vector(a_long * col(channel_W, v)');
		}

		mu = rows_dot_product(X, aW[,voxel]');
		vox_avg = calc_vox_avg(mu, n_voxel, max_nv, n, voxel, n_v_real);

		y ~ normal(mu - vox_avg[voxel], sigma);
	}
}
