data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_basis * n_channels * n_contrast] X; // cycle: basis, contrast, channel, voxel
	real<lower=0> a_min;
	real<lower=1> nu_global;
	real<lower=1> nu_local;
	real<lower=0> scale_global;
	real slab_scale; // expected scale of large channels
	real slab_df;
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	row_vector[n_contrast] zz = rep_row_vector(0.0, n_contrast);
	int K =  cols(X);
	int D = n_channels * n_voxel;
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;
	real half_slab_df = 0.5*slab_df;
	real half_nu_global = 0.5*nu_global;
	real half_nu_local = 0.5*nu_local;

	for(i in 1:n){
		n_v_int[voxel[i]] = n_v_int[voxel[i]] + 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);

}
parameters {
	real<lower=0.0>sigma;
	matrix[n_basis - 1, n_contrast] a_raw; //
	vector<lower=0>[D] z; // latent beta. unsure about forcing to be positive
	real<lower=0> caux;
	real<lower=0> aux1_global;
	real<lower=0> aux2_global;
	vector<lower=0>[D] aux1_local;
	vector<lower=0>[D] aux2_local;
}
transformed parameters{
	matrix<lower=0, upper = 1>[n_basis, n_contrast] a;
	vector<lower=0>[D] lambda = aux1_local .* sqrt(aux2_local); // local shrinkage parameters
	real<lower=0> tau = aux1_global * sqrt(aux2_global) * scale_global * sigma; // global shrinkage parameters
	real<lower=0> c = slab_scale * sqrt(caux);
	vector<lower=0>[D] lambda_tilde = sqrt(c^2 * square(lambda) ./ (c^2 + tau^2 * square(lambda)) );
	vector<lower=0>[D] beta = z .* lambda*tau;

  {
		matrix[n_basis, n_contrast] a_raw_full = append_row( zz, a_raw);
		a[, 1] = cumulative_sum(softmax(col(a_raw_full, 1)));
		a[, 2] = cumulative_sum(softmax(col(a_raw_full, 2)));
	}

}
model{

	// half-t priors for lambdas and tau, and inverse-gamma for c^2
	z ~ normal(0, 1);
	aux1_local ~ normal(0, 1);
	aux2_local ~ inv_gamma(half_nu_local, half_nu_local);
	aux1_global ~ normal(0, 1);
	aux2_global ~ inv_gamma(half_nu_global, half_nu_global);
	caux ~ inv_gamma(half_slab_df, half_slab_df);

	to_vector(a_raw) ~ normal(0, priors[3]);
	sigma ~ gamma(2, priors[7]);

	// likelihood
	{
		vector[n_contrast * n_basis] a_long = to_vector(a);
		matrix[n_channels, n_voxel] channel_W = to_matrix(beta, n_channels, n_voxel);
		matrix[K, n_voxel] aW;
		vector[n] mu;
		vector[max_nv] vox_sum;
		vector[n_voxel] vox_avg;
		int j;

		for(v in 1:n_voxel){
			aW[, v] = to_vector(a_long * col(channel_W, v)');
		}

		mu = rows_dot_product(X, aW[,voxel]');

		for(v in 1:n_voxel){
			vox_sum = rep_vector(0, max_nv);
			j = 1;
			for(i in 1:n){
				if(voxel[i] == v){
					vox_sum[j] = mu[i];
					j = j + 1;
				}
			}
			vox_avg[v] = sum(vox_sum) / n_v_real[v];
		}

		y ~ normal(mu - vox_avg[voxel], sigma);
	}
}
