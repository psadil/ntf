functions{
#include /functions/common_functions.stan
}
data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_basis * n_channels * n_contrast] X; // cycle: basis, contrast, channel, voxel
	real<lower=0> a_max;
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	int K =  cols(X);
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;

	for(i in 1:n){
		n_v_int[voxel[i]] = n_v_int[voxel[i]] + 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);

}
parameters {
	vector<lower = 0>[n_voxel] voxel_b_raw;
	matrix[n_channels-1, n_voxel] channel_W_raw;
	// real<lower = 0> channel_sigma;
	real<lower = 0> voxel_sigma;
	real<lower=0> sigma;
	// positive_ordered[n_basis - 1] a_raw_low; //
	// positive_ordered[n_basis - 1] a_raw_high; //
	matrix<lower=0, upper=1>[n_basis - 1, n_contrast] a_raw;
	real<lower=0.0> a_high1;
}
transformed parameters{
	vector<lower = 0>[n_voxel] voxel_b = voxel_b_raw * voxel_sigma;
	matrix<lower=0.0>[n_channels, n_voxel] channel_W;
	matrix<lower=0.0>[n_basis, n_contrast] a;

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = softmax(col(channel_W_tmp, v)) * voxel_b[v];
	}

  a[4, 1] = a_max;
	a[4, 2] = a_high1;
	for(i in 1:3){
		a[4-i, 1] = a_max .* prod(a_raw[1:i, 1]);
		a[4-i, 2] = a_high1 .* prod(a_raw[1:i, 2]);
	}

}
model{

	// prior
	// channel_sigma ~ normal(0, priors[5]);
	to_vector(channel_W_raw) ~ normal(0, priors[5]);

	voxel_sigma ~ gamma(2, priors[2]);
	voxel_b_raw ~ normal(0, 1);

  to_vector(a_raw) ~ beta(priors[3], priors[3]);
	// a_raw_low ~ normal(0, priors[3]);
	// a_raw_high ~ normal(0, priors[3]);
	a_high1 ~ lognormal(priors[8], priors[9]);
	sigma ~ gamma(2, priors[7]);

	// likelihood
	{
		vector[n_contrast * n_basis] a_long = to_vector(a);
		matrix[K, n_voxel] aW;
		vector[n] mu;
		vector[n_voxel] vox_avg;

		for(v in 1:n_voxel){
			aW[, v] = to_vector(a_long * col(channel_W, v)');
		}

		mu = rows_dot_product(X, aW[,voxel]');
		vox_avg = calc_vox_avg(mu, n_voxel, max_nv, n, voxel, n_v_real);

		y ~ normal(mu - vox_avg[voxel], sigma);
	}
}
