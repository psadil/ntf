data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	int<lower = 1> n_basis; // even (14)
	vector[n] y;
	matrix[n, n_basis * n_channels * n_contrast] X; // cycle: basis, contrast, channel, voxel
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	row_vector[n_contrast] zz = rep_row_vector(0.0, n_contrast);
	int K =  cols(X);
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;
	matrix[n, K] X_centered;
	matrix[n, K] Q;
	matrix[K, K] R;
	matrix[K, K] R_inv;

	for(k in 1:K) col(X_centered, k) = col(X,k) - mean(col(X,k));

	Q = qr_Q(X_centered)[, 1:K] * n;
	R = qr_R(X_centered)[1:K, ] / n;
	R_inv = inverse(R);

	for(i in 1:n){
		n_v_int[voxel[i]] = n_v_int[voxel[i]] + 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);
}
parameters {
	matrix[n_channels-1, n_voxel] channel_W_raw;
	real<lower=0> sigma;
	vector<lower=0>[n_voxel] multiplicative_constant;
	vector[K] a_tilde;
}
transformed parameters{
	matrix[n_channels, n_voxel] channel_W;
	matrix[K, n_voxel] aW;
	vector[n_contrast * n_basis] a;

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = softmax(col(channel_W_tmp, v)) * multiplicative_constant[v];
	}

	for(v in 1:n_voxel){
		aW[, v] = R_inv * to_vector(a_tilde * col(channel_W, v)');
	}

}
model{

	// prior
	to_vector(channel_W_raw) ~ normal(0, priors[1]);
	multiplicative_constant ~ gamma(2, priors[2]);

	a ~ exponential(priors[3]);
	sigma ~ gamma(2, priors[7]);

	// likelihood
	y ~ normal(Q * a_tilde, sigma);
}
