#include /chunks/license.stan

functions{
	// vector calc_vox_avg(vector mu, int n_voxel, int max_nv, int n, int[] voxel, vector n_v_real);
#include /functions/common_functions.stan
	// vector von_mises_pdf(vector ori_tested, real ori_pref, real kappa );
#include /functions/channel_responses.stan
}
data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested; // 9
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	vector[n] y;
	matrix[n, n_orientation_tested * n_contrast * n_channels ] X;
	vector<lower=-pi(), upper=pi()>[n_channels] pref_ori;
	matrix[n_orientation_tested, n_channels] unique_orientation_tested;
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	row_vector[n_contrast] zz = rep_row_vector(0.0, n_contrast);
	int K =  cols(X);
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;

	for(i in 1:n){
		n_v_int[voxel[i]] += 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);

}
parameters {
	vector<lower = 0, upper = 1>[n_voxel] voxel_b;
	matrix[n_channels-1, n_voxel] channel_W_raw;
	// real<lower = 0> channel_sigma;
	// real<lower = 0> voxel_sigma;
	real<lower = 0> sigma;
	vector<lower = 1>[n_contrast] m;
	vector<lower = 0>[n_contrast] s;
	vector[n_contrast] a;
	real<lower=0, upper=1> mode;
	real<lower=0> concentration;
}
transformed parameters {
	// vector<lower = 0>[n_voxel] voxel_b = voxel_b_raw * voxel_sigma;
	matrix[n_channels, n_voxel] channel_W;

	{
		matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
		for(v in 1:n_voxel) channel_W[,v] = softmax(col(channel_W_tmp, v)) * voxel_b[v];
	}

}
model{

	// prior
	m ~ normal(1, priors[1]);
	s ~ lognormal(priors[8], priors[9]);
	a ~ normal(0, priors[4]);

	// channel_sigma ~ normal(0, priors[5]);
	to_vector(channel_W_raw) ~ normal(0, priors[5]);
	// voxel_sigma ~ gamma(2, priors[2]);
	// voxel_b_raw ~ normal(0, 1);
	mode ~ beta(priors[2], priors[2]);
	concentration ~ gamma(2, priors[3]);
	voxel_b ~ beta(calc_alpha(mode, concentration+2), calc_beta(mode, concentration+2));

	sigma ~ gamma(2, priors[7]);

	// likelihood
	{
		vector[n_orientation_tested] channel_resp;
		matrix[K, n_voxel] aW;
		int i;
		int j;

		for(v in 1:n_voxel){
			i = 1;
			j = 0;
			for(ch in 1:n_channels){
				for(con in 1:n_contrast){
					j += n_orientation_tested;

					channel_resp = m[con] * von_mises_pdf(unique_orientation_tested[, ch], pref_ori[ch], s[con]) + a[con];
					aW[i:j, v] = channel_resp * channel_W[ch, v];

					i += n_orientation_tested;
				}
			}
		}

		y ~ normal(rows_dot_product(X, aW[, voxel]'), sigma);
	}
}
