#include /chunks/license.stan

functions{
#include /functions/common_functions.stan
#include /functions/channel_responses.stan
}
data {
	int n;
	int n_voxel;
	int n_channels;
	int n_contrast;
	int n_orientation_tested; // 9
	int contrast[n];
	int voxel[n];
	vector[9] priors;
	vector[n] y;
	matrix[n, n_orientation_tested * n_contrast * n_channels ] X;
	vector<lower=-pi(), upper=pi()>[n_channels] pref_ori;
	matrix[n_orientation_tested, n_channels] unique_orientation_tested;
}
transformed data {
	row_vector[n_voxel] zeros = rep_row_vector(0.0, n_voxel);
	row_vector[n_contrast] zz = rep_row_vector(0.0, n_contrast);
	int K =  cols(X);
	int n_v_int[n_voxel]  = rep_array(0, n_voxel);
	vector[n_voxel] n_v_real;
	int max_nv;

	for(i in 1:n){
		n_v_int[voxel[i]] = n_v_int[voxel[i]] + 1;
	}

	n_v_real = to_vector(n_v_int);
	max_nv = max(n_v_int);

}
parameters {
	vector<lower = 0>[n_voxel] voxel_b_raw;
	matrix<lower = 0>[n_channels-1, n_voxel] channel_W_raw;
	real<lower = 0> voxel_sigma;
	real<lower = 0> sigma;
	real<lower = 0> m;
	vector<lower = 0>[n_contrast] s;
	real a;
	real<lower=0> caux;
	real<lower=0> aux1_global;
	real<lower=0> aux2_global;
	vector<lower=0>[D] aux1_local;
	vector<lower=0>[D] aux2_local;
}
transformed parameters {
  vector<lower = 0>[n_voxel] voxel_b = voxel_b_raw * voxel_sigma;
	matrix[n_channels, n_voxel] channel_W;

		{
			matrix[n_channels, n_voxel] channel_W_tmp = append_row(channel_W_raw, zeros);
			for(v in 1:n_voxel) channel_W[,v] = exp(log_softmax(col(channel_W_tmp, v))) * voxel_b[v];
		}

}
model{

	// prior
	m ~ normal(0, priors[1]);
	s ~ normal(0, priors[3]);
	a ~ normal(0, priors[4]);

  to_vector(channel_W_raw) ~ normal(0, priors[5]);
  voxel_sigma ~ gamma(2, priors[2]);
	voxel_b_raw ~ normal(0, 1);

	sigma ~ gamma(2, priors[7]);

	// likelihood
	{
		vector[n] mu;
		matrix[n_orientation_tested, n_contrast] channel_resp;
		matrix[K, n_voxel] aW;
		vector[n_voxel] vox_avg;

    for(ch in 1:n_channels){
			channel_resp[, 1] = von_mises_pdf(unique_orientation_tested[, ch], pref_ori[ch], s[1]);
			channel_resp[, 2] = (m * von_mises_pdf(unique_orientation_tested[, ch], pref_ori[ch], s[2])) + a;

			for(v in 1:n_voxel){
				aW[, v] = to_vector(to_vector(channel_resp) * col(channel_W, v)');
			}
		}

		mu = rows_dot_product(X, aW[, voxel]');
		vox_avg = calc_vox_avg(mu, n_voxel, max_nv, n, voxel, n_v_real);

		y ~ normal(mu - vox_avg[voxel], sigma);
	}
}
